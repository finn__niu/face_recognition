1. pip install -r requirements.txt
2. Use extract_face.py extract target face from a video, save extracted face to a file
file structure like this:
face_dataset
----LabelName1
---------image1.png
---------image2.png
----LabelName2
----LabelName3
...
3. Change dataset path in train_fr_svm.py, and update your label map.
4. Update your label map in test.pt and then test your svm model. 