import face_recognition
import cv2
import os
# os.environ["CUDA_VISIBLE_DEVICES"]="-1" 
img_sv_dir='/media/apptech/golden/dataset/face/yrx/'
video_dir='/home/apptech/pixeleye/input/yrx1.mp4'
cam=cv2.VideoCapture(video_dir)
face_locations = []
cnt=0
while True:
    ret,frame=cam.read()
    frame=frame[:,250:,::]
    if not ret:continue
    face_locations = face_recognition.face_locations(frame,model='cnn')
    print(face_locations)
    if face_locations:
      top, right, bottom, left=face_locations[0][0],face_locations[0][1],face_locations[0][2],face_locations[0][3]
      face_image=frame[top-30:bottom+10,left-10:right+10]
      cv2.imwrite(f'{img_sv_dir}{cnt}.png',face_image)
      cv2.rectangle(frame,(left-2,top-2),(right+2,bottom+2),(0,0,255),1)

      cnt+=1
    if cnt==1000:
      break
    cv2.imshow('',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
            break
