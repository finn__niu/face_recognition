import cv2
import face_recognition
from sklearn.svm import SVC
import pickle
import os
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import pickle
#dataset path
dataset_dir='/media/apptech/golden/dataset/face/'

x_train=[]
y_train=[]
x_test=[]
y_test=[]
#add name here
label_list={
    'cf':0,
    'cmb':1,
    'czs':2,
    'lz':3,
    'qth':4,
    'yrx':5,
    'zjz':6
}
tuned_parameters = [{'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
scores = ['precision', 'recall']
def extract_features():
    labels=os.listdir(dataset_dir)
    for label in labels:
        image_dir=os.listdir(dataset_dir+label)
        cnt=0
        print(label)
        for img in image_dir:
            try:
                image=cv2.imread(f'{dataset_dir+label}/{img}')
                face_locations = face_recognition.face_locations(image)
                face_encodings = face_recognition.face_encodings(image, face_locations)
                if cnt<=800:
                    x_train.append(list(face_encodings[0]))
                    y_train.append(label_list[f'{label}'])
                else:
                    x_test.append(list(face_encodings[0]))
                    y_test.append(label_list[f'{label}'])
                cnt+=1
            except:
                print('image error')
                continue
    print(f'len(x_train){len(x_train)},len(x_test){len(x_test)}')
    print(x_train[0],y_train[0])
        
def svm_model(scores,x_train,y_train,x_test,y_test,tuned_parameters):
    svc = SVC(C=1.0,kernel='linear', probability=True,class_weight='balanced',verbose=True)
    svc.fit(x_train,y_train)

    if not os.path.exists('save'):
        os.makedirs('save')
        f=open('save/clf.pickle','w')
        f.close()
    with open('save/clf.pickle', 'wb') as f:
        pickle.dump(svc, f)

if __name__ == '__main__':
    extract_features()
    svm_model(scores,x_train,y_train,x_test,y_test,tuned_parameters)