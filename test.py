
from sklearn.svm import SVC
import pickle
import cv2
import face_recognition
import numpy as np
label_list={
    'cf':0,
    'cmb':1,
    'czs':2,
    'lz':3,
    'qth':4,
    'yrx':5,
    'zjz':6
}
label_map={
    0:'Frank Chan Fan',
    1:'Can Mau Bo',
    2:'Sophia Chan Siu-chee',
    3:'Carrie Lam Cheng',
    4:'Jau Tang Waa',
    5:'Kevin Yeung Yun-hung',
    6:'Zoeng Gin Zung'
}
with open('save/clf.pickle', 'rb') as f:
    clf = pickle.load(f)
    cam=cv2.VideoCapture(0)
    while True:

        success,image=cam.read()
        image=cv2.flip(image,1)
        if not success:continue
        # image=cv2.imread('lz.png')
        face_locations = face_recognition.face_locations(image)
        face_encodings = face_recognition.face_encodings(image, face_locations)
        for (top, right, bottom, left),f_e in zip(face_locations,face_encodings):
            cv2.rectangle(image, (left, top), (right, bottom), (0, 0, 255), 2)
            classes=clf.predict(list(face_encodings))
            score=max(clf.predict_proba(list(face_encodings))[0])
            print(score)
            

            # print(classes,score)

            if score>=0.87:
                name=label_map[classes[0]]
                print(name)
                cv2.putText(image,f'{str(name)}', (left,top-5),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
            else:
                cv2.putText(image,'Unknown', (left,top-5),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)


        image=cv2.resize(image,(1280,720))
        cv2.imshow('result', image)
  
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cam.release()
    

    #测试读取后的Model
    # print(clf2.predict(X[0:1]))